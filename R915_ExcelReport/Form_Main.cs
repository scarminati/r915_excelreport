﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Drawing;

namespace R915_ExcelReport
{
    public partial class Form_Main : Form
    {
        public Form_Main()
        {
            InitializeComponent();
            setWidgetDescriptions();
        }

        private void setWidgetDescriptions()
        {
            try
            {
                #region TooltipsPerBottoni
                ToolTip tooltip = new System.Windows.Forms.ToolTip();
                tooltip.SetToolTip(btn_AddFile, "Aggiungi File");
                tooltip.SetToolTip(btn_GeneraReport, "Genera Report");
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.Multiselect = true;
                dlg.Filter = "File Report | *.csv";
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    for (int i = 0; i < dlg.FileNames.Length; i++)
                        dgv_listaFile.Rows.Insert(0, dlg.FileNames[i].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_GeneraReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgv_listaFile.Rows.Count.Equals(0))
                    MessageBox.Show("Seleziona almeno un file da convertire", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (cmb_operatore.SelectedItem == null)
                    MessageBox.Show("Seleziona l'operatore", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    pb_generazReport.Maximum = dgv_listaFile.Rows.Count;
                    pb_generazReport.Value = 0;
                    List<String[]> dataFromCSV;
                    foreach (DataGridViewRow row in dgv_listaFile.Rows)
                    {
                        dataFromCSV = new List<string[]>();
                        dataFromCSV = getDataFromCSVFile(row.Cells["nomeFile"].Value.ToString());
                        generateReportXLS(Path.GetFileName(row.Cells["nomeFile"].Value.ToString()), dataFromCSV);
                        pb_generazReport.PerformStep();
                    }
                    MessageBox.Show("Generazione Report Completata", "Genera Report", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Legge tutte le linee del file di input ed aggiunge le stringhe opportune, necessarie per il file excel.
        /// </summary>
        /// <param name="fileName">Nome del file</param>
        /// <returns></returns>
        private List<String[]> getDataFromCSVFile(string fileName)
        {
            try
            {
                List<string[]> retValue = new List<string[]>();
                String[] lineFromFile;
                String[] dataFromLine;
                lineFromFile = File.ReadAllLines(fileName);
                lineFromFile[0] = lineFromFile[0] + "Operator;Signature;";
                for (int i = 1; i <= 2; i++)
                    lineFromFile[i] = lineFromFile[i] + ";;";
                for (int i = 3; i < lineFromFile.Length; i++)
                    lineFromFile[i] = lineFromFile[i] + cmb_operatore.SelectedItem.ToString() + ";;";
                for (int i = 0; i < lineFromFile.Length; i++)
                {
                    dataFromLine = lineFromFile[i].Split(';');
                    retValue.Add(dataFromLine);
                }
                return retValue;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private void generateReportXLS(String fileName, List<String[]> dataFromCSV)
        {
            try
            {
                //string path = Environment.CurrentDirectory + @"\template\templateReport.xls";
                //FileInfo fi = new FileInfo(path);
                ExcelPackage excel = new ExcelPackage();
                //Add the worksheet Report
                excel.Workbook.Worksheets.Add(fileName.Replace(".csv", ""));
                // Determine the header range (e.g. A1:D1)
                string headerRange = "A1:" + Char.ConvertFromUtf32(dataFromCSV[0].Length + 64) + "1";
                // Target a worksheet
                var worksheet = excel.Workbook.Worksheets[fileName.Replace(".csv", "")];
                // Popular header row data
                worksheet.Cells[headerRange].LoadFromArrays(dataFromCSV);
                //insert the signature
                Image signature = Image.FromFile(Environment.CurrentDirectory + @"\img\" + cmb_operatore.SelectedItem.ToString().Replace(" ", "") + ".jpg");
                for (int i = 3; i < dataFromCSV.Count; i++)
                {
                    ExcelPicture pic = worksheet.Drawings.AddPicture("Pic " + i, signature);
                    pic.SetPosition(i, 5, dataFromCSV[0].Length - 2, 5);
                    pic.SetSize(100);
                }
                //insert the last comment
                worksheet.Cells["C9"].Style.Font.Bold = true;
                worksheet.Cells["C9"].Style.Font.Size = 14;
                worksheet.Cells["C9"].LoadFromText("The test is applied with R915 S DHHS Collimator that integrates Ag filter in the place of Rh on other type of collimator");
                //edit J5 Column
                worksheet.Cells["J5"].IsRichText = true;
                ExcelRichText richtext = worksheet.Cells["J5"].RichText.Add("Rh");
                richtext.Strike = true;
                richtext = worksheet.Cells["J5"].RichText.Add(" Ag");
                richtext.Strike = false;
                //set cells layout
                worksheet.Cells[1, 1, dataFromCSV.Count, dataFromCSV[0].Length - 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[3, 1, dataFromCSV.Count, dataFromCSV[0].Length - 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[1, 1, dataFromCSV.Count, dataFromCSV[0].Length - 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[1, 1, dataFromCSV.Count, dataFromCSV[0].Length - 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[1, 1, dataFromCSV.Count, dataFromCSV[0].Length - 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells[1, 1, dataFromCSV.Count, dataFromCSV[0].Length - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[1, 1, dataFromCSV.Count, dataFromCSV[0].Length - 1].AutoFitColumns();
                worksheet.PrinterSettings.Orientation = eOrientation.Landscape;
                worksheet.PrinterSettings.FitToPage = true;

                for (int i = 4; i <= dataFromCSV.Count; i++)
                {
                    worksheet.Row(i).Height = 45;
                    worksheet.Column(dataFromCSV[0].Length - 1).Width = 32;
                }
                //save the excel file report
                if (!Directory.Exists(Environment.CurrentDirectory + @"\reports"))
                    Directory.CreateDirectory(Environment.CurrentDirectory + @"\reports\");
                FileInfo excelFile = new FileInfo(Environment.CurrentDirectory + @"\reports\" + fileName.Replace(".csv",".xlsx"));
                excel.SaveAs(excelFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
