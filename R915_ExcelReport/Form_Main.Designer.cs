﻿namespace R915_ExcelReport
{
    partial class Form_Main
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pb_generazReport = new System.Windows.Forms.ProgressBar();
            this.btn_GeneraReport = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.cmb_operatore = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_AddFile = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgv_listaFile = new System.Windows.Forms.DataGridView();
            this.nomeFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listaFile)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pb_generazReport);
            this.groupBox1.Controls.Add(this.btn_GeneraReport);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.cmb_operatore);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btn_AddFile);
            this.groupBox1.Location = new System.Drawing.Point(15, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(668, 83);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // pb_generazReport
            // 
            this.pb_generazReport.Location = new System.Drawing.Point(518, 33);
            this.pb_generazReport.Name = "pb_generazReport";
            this.pb_generazReport.Size = new System.Drawing.Size(111, 23);
            this.pb_generazReport.TabIndex = 19;
            // 
            // btn_GeneraReport
            // 
            this.btn_GeneraReport.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_GeneraReport.BackgroundImage")));
            this.btn_GeneraReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_GeneraReport.Location = new System.Drawing.Point(437, 19);
            this.btn_GeneraReport.Name = "btn_GeneraReport";
            this.btn_GeneraReport.Size = new System.Drawing.Size(50, 50);
            this.btn_GeneraReport.TabIndex = 18;
            this.btn_GeneraReport.UseVisualStyleBackColor = true;
            this.btn_GeneraReport.Click += new System.EventHandler(this.btn_GeneraReport_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(242, 19);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(18, 50);
            this.pictureBox2.TabIndex = 17;
            this.pictureBox2.TabStop = false;
            // 
            // cmb_operatore
            // 
            this.cmb_operatore.FormattingEnabled = true;
            this.cmb_operatore.Items.AddRange(new object[] {
            "Minutillo Danilo",
            "Modica Paolo"});
            this.cmb_operatore.Location = new System.Drawing.Point(94, 47);
            this.cmb_operatore.Name = "cmb_operatore";
            this.cmb_operatore.Size = new System.Drawing.Size(121, 21);
            this.cmb_operatore.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(91, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Operatore";
            // 
            // btn_AddFile
            // 
            this.btn_AddFile.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_AddFile.BackgroundImage")));
            this.btn_AddFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_AddFile.Location = new System.Drawing.Point(15, 19);
            this.btn_AddFile.Name = "btn_AddFile";
            this.btn_AddFile.Size = new System.Drawing.Size(50, 50);
            this.btn_AddFile.TabIndex = 7;
            this.btn_AddFile.UseVisualStyleBackColor = true;
            this.btn_AddFile.Click += new System.EventHandler(this.btn_AddFile_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgv_listaFile);
            this.groupBox2.Location = new System.Drawing.Point(12, 101);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(668, 542);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // dgv_listaFile
            // 
            this.dgv_listaFile.AllowUserToAddRows = false;
            this.dgv_listaFile.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_listaFile.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nomeFile});
            this.dgv_listaFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_listaFile.Location = new System.Drawing.Point(3, 16);
            this.dgv_listaFile.Name = "dgv_listaFile";
            this.dgv_listaFile.Size = new System.Drawing.Size(662, 523);
            this.dgv_listaFile.TabIndex = 0;
            // 
            // nomeFile
            // 
            this.nomeFile.HeaderText = "Nome File";
            this.nomeFile.Name = "nomeFile";
            this.nomeFile.Width = 600;
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(692, 655);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximumSize = new System.Drawing.Size(708, 693);
            this.MinimumSize = new System.Drawing.Size(708, 693);
            this.Name = "Form_Main";
            this.Text = "R915 Excel Report";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listaFile)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgv_listaFile;
        private System.Windows.Forms.Button btn_GeneraReport;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ComboBox cmb_operatore;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_AddFile;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeFile;
        private System.Windows.Forms.ProgressBar pb_generazReport;
    }
}

